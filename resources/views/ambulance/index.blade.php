<html>
    <head>
        <title>Ambulance List</title>
    </head>
    <body>
        <h3>Ambulance List</h3>
        <table border="1" cellspacing="0" cellpadding="3">
            <tr>
                <td>SL</td>
                <td>Name</td>
                <td>Phone</td>
                <td>Action</td>
            </tr>
            @php $i=0; @endphp
            @foreach($ambulances as $ambulance)
                <td>{{++$i}}</td>
                <td>{{$ambulance->name}}</td>
                <td>{{$ambulance->phone}}</td>
                <td>
                    <form action="{{url('ambulance/edit/'.$ambulance->id)}}" method="get">
                        <input type="submit" value="Edit">
                    </form>
                </td>
            @endforeach
        </table>
    </body>
</html>