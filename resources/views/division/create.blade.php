<!DOCTYPE html>
<html>
<head>
    <title>Add Division</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
</head>
<body>

<div id="pac-container">
    @include('layout.fixed')
    <h3>Add Division</h3>
    <form method="post" action="{{url('division/store')}}">
        {{ csrf_field() }}
        <label for="name">Name</label>
        <input name="name" type="text" />
        <input type="submit">
    </form>
</div>
<h3>Divisions</h3>
<table border="1" cellspacing="0" cellpadding="3">
    <tr>
        <td>SL</td>
        <td>Name</td>
        <td>Action</td>
    </tr>
    @php $i=0; @endphp
    @foreach($divisions as $division)
        <tr>
        <td>{{++$i}}</td>
        <td>{{$division->name}}</td>
        <td>
            <form action="{{url('division/edit/'.$division->id)}}" method="get">
                <input type="submit" value="Edit">
            </form>
            <form action="{{url('division/delete/'.$division->id)}}" method="post">{{csrf_field()}}
                <input type="submit" value="Delete" >
            </form>
        </td>
        </tr>
    @endforeach
</table>
</body>
</html>