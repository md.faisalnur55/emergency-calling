<html>
    <head>
        <title>Rab List</title>
    </head>
    <body>
        <h3>Rab List</h3>
        <table border="1" cellspacing="0" cellpadding="3">
            <tr>
                <td>SL</td>
                <td>Name</td>
                <td>Phone</td>
                <td>Action</td>
            </tr>
            @php $i=0; @endphp
            @foreach($rabs as $rab)
                <td>{{++$i}}</td>
                <td>{{$rab->name}}</td>
                <td>{{$rab->phone}}</td>
                <td>
                    <form action="{{url('rab/edit/'.$rab->id)}}" method="get">
                        <input type="submit" value="Edit">
                    </form>
                </td>
            @endforeach
        </table>
    </body>
</html>