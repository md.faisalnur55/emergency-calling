<html>
    <head>
        <title>Fire Service List</title>
    </head>
    <body>
        <h3>Fire Service List</h3>
        <table border="1" cellspacing="0" cellpadding="3">
            <tr>
                <td>SL</td>
                <td>Name</td>
                <td>Phone</td>
                <td>Action</td>
            </tr>
            @php $i=0; @endphp
            @foreach($fire_services as $fire_service)
                <td>{{++$i}}</td>
                <td>{{$fire_service->name}}</td>
                <td>{{$fire_service->phone}}</td>
                <td>
                    <form action="{{url('fire_service/edit/'.$fire_service->id)}}" method="get">
                        <input type="submit" value="Edit">
                    </form>
                </td>
            @endforeach
        </table>
    </body>
</html>