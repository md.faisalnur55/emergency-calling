<html>
    <head>
        <title>Police List</title>
    </head>
    <body>
        <h3>Police List</h3>
        <table border="1" cellspacing="0" cellpadding="3">
            <tr>
                <td>SL</td>
                <td>Name</td>
                <td>Phone</td>
                <td>Action</td>
            </tr>
            @php $i=0; @endphp
            @foreach($polices as $police)
                <td>{{++$i}}</td>
                <td>{{$police->name}}</td>
                <td>{{$police->phone}}</td>
                <td>
                    <form action="{{url('police/edit/'.$police->id)}}" method="get">
                        <input type="submit" value="Edit">
                    </form>
                </td>
            @endforeach
        </table>
    </body>
</html>