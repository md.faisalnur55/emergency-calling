<html>
    <head>
        <title>Hospital List</title>
    </head>
    <body>
        <h3>Hospital List</h3>
        <table border="1" cellspacing="0" cellpadding="3">
            <tr>
                <td>SL</td>
                <td>Name</td>
                <td>Phone</td>
                <td>Action</td>
            </tr>
            @php $i=0; @endphp
            @foreach($hospitals as $hospital)
                <td>{{++$i}}</td>
                <td>{{$hospital->name}}</td>
                <td>{{$hospital->phone}}</td>
                <td>
                    <form action="{{url('hospital/edit/'.$hospital->id)}}" method="get">
                        <input type="submit" value="Edit">
                    </form>
                </td>
            @endforeach
        </table>
    </body>
</html>