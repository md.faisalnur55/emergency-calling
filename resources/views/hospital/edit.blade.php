<!DOCTYPE html>
<html>
<head>
    <title>Add Hospital</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">


    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            margin-top: 30px;
            height: 500px;
            width: 900px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }

        #type-selector{
            margin-top: 50px;
        }
    </style>
</head>
<body>
<div>@include('layout.fixed')</div>
<div class="pac-containers" id="pac-card">

    <div>
        <div id="title">
            Enter your place
        </div>
        <div id="type-selector" class="pac-controls" style="display: none">
            <!--            <input type="radio" name="type" id="changetype-all" checked="checked">-->
            <!--            <label for="changetype-all">All</label>-->
            <!---->
            <!--            <input type="radio" name="type" id="changetype-establishment">-->
            <!--            <label for="changetype-establishment">Establishments</label>-->
            <!---->
            <!--            <input type="radio" name="type" id="changetype-address">-->
            <!--            <label for="changetype-address">Addresses</label>-->

            <input type="radio" name="type" id="changetype-geocode" checked="checked">
            <label for="changetype-geocode">Geocodes</label>
        </div>
        <!--        <div id="strict-bounds-selector" class="pac-controls">-->
        <!--            <input type="checkbox" id="use-strict-bounds" value="">-->
        <!--            <label for="use-strict-bounds">Strict Bounds</label>-->
        <!--        </div>-->
    </div>

</div>

<div id="map"></div><br><br>
<div id="pac-container">
    <form method="post" action="{{url('hospital/update/'.$hospital->id)}}">

        <!--<input type="hidden" name="user_id" id="user_id" value=""/>-->
        <label for="changetype-geocode">Name :</label>
        <input name="name" type="text" value="{{$hospital->name}}"/><br><br>

        <label for="changetype-geocode">Type Location :</label>
        <input id="pac-input" type="text" placeholder="Enter a location" /><br><br>

        <label for="changetype-geocode">Division :</label>
        <select name="division_id">
            <option>Select Division</option>
            @foreach($divisions as $division)
                <option value="{{$division->id}}" @php if($hospital->division_id == $division->id){echo "selected"; } @endphp>{{$division->name}}</option>
            @endforeach
        </select>
        <br><br>

        <label for="changetype-geocode">District :</label>
        <select name="division_id">
            <option>Select District</option>
            @foreach($districts as $district)
                <option value="{{$district->id}}" @php if($hospital->district_id == $district->id){echo "selected"; } @endphp >{{$district->name}}</option>
            @endforeach
        </select>
        <br><br>
        <input name="longitude" id="longitude" type="hidden" value="{{$hospital->longitude}}" />
        <input name="latitude" id="latitude" type="hidden" value="{{$hospital->latitude}}" />

        <label for="changetype-geocode">Phone</label>
        <input name="phone" type="text" value="{{$hospital->phone}}" /><br><br>

        <input type="submit">
    </form>
</div>
<div id="infowindow-content">
    <img src="" width="16" height="16" id="place-icon">
    <span id="place-name"  class="title"></span><br>
    <span id="place-address"></span>
</div>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">


    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);

            var longitude = place.geometry.location.lng();
            var latitude = place.geometry.location.lat();

            $('#longitude').val(longitude);
            $('#latitude').val(latitude);

        });

        // Sets a listener on a radio button to change the filter type on Places
//        // Autocomplete.
//        function setupClickListener(id, types) {
//            var radioButton = document.getElementById(id);
//            radioButton.addEventListener('click', function() {
//                autocomplete.setTypes(types);
//            });
//        }
//
//        setupClickListener('changetype-all', []);
//        setupClickListener('changetype-address', ['address']);
//        setupClickListener('changetype-establishment', ['establishment']);
//        setupClickListener('changetype-geocode', ['geocode']);


        // document.getElementById('use-strict-bounds')
        //     .addEventListener('click', function() {
        //         console.log('Checkbox clicked! New state=' + this.checked);
        //         autocomplete.setOptions({strictBounds: this.checked});
        //     });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAactlxvEZ4R7LWZnHy0yQfESSL1jhrOCA&libraries=places&callback=initMap"
        async defer></script>
<script>
    $(document).ready(function () {
        var address = window.location.href;
        var user_id = address[address.length -1];
//        alert(user_id);
        // $('#user_id').val(user_id);
    })
</script>

</body>
</html>