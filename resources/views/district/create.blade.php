<!DOCTYPE html>
<html>
<head>
    <title>Add District</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
</head>
<body>

<div id="pac-container">
    @include('layout.fixed')
    <h3>Add District</h3>
    <form method="post" action="{{url('district/store')}}">
        {{ csrf_field() }}
        <label for="name">Division</label>
        <select name="division_id">
            <option>Select Division</option>
            @foreach($divisions as $division)
                <option value="{{$division->id}}">{{$division->name}}</option>
            @endforeach
        </select>
        <br /><br />
        <label for="name">Name</label>
        <input name="name" type="text" />
        <input type="submit">
    </form>
</div>
</body>
</html>