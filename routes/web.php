<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/** Division Routes  */
Route::get('divisions','DivisionController@index');
Route::get('division/create','DivisionController@create');
Route::post('division/store','DivisionController@store');
Route::get('division/edit/{id}','DivisionController@edit');
Route::patch('division/update/{id}','DivisionController@update');
Route::post('division/delete/{id}','DivisionController@destroy');

/** District Routes  */
Route::get('districts','DistrictController@index');
Route::get('district/create','DistrictController@create');
Route::post('district/store','DistrictController@store');
Route::get('district/edit/{id}','DistrictController@edit');
Route::patch('district/update/{id}','DistrictController@update');
Route::delete('district/delete/{id}','DistrictController@destroy');

/** Ambulance Routes  */
Route::get('ambulances','AmbulanceController@index');
Route::get('ambulance/create','AmbulanceController@create');
Route::post('ambulance/store','AmbulanceController@store');
Route::get('ambulance/edit/{id}','AmbulanceController@edit');
Route::patch('ambulance/update/{id}','AmbulanceController@update');
Route::delete('ambulance/delete/{id}','AmbulanceController@destroy');


/** FireService Routes  */
Route::get('fire_services','FireServiceController@index');
Route::get('fire_service/create','FireServiceController@create');
Route::post('fire_service/store','FireServiceController@store');
Route::get('fire_service/edit/{id}','FireServiceController@edit');
Route::patch('fire_service/update/{id}','FireServiceController@update');
Route::delete('fire_service/delete/{id}','FireServiceController@destroy');


/** Hospital Routes  */
Route::get('hospitals','HospitalController@index');
Route::get('hospital/create','HospitalController@create');
Route::post('hospital/store','HospitalController@store');
Route::get('hospital/edit/{id}','HospitalController@edit');
Route::patch('hospital/update/{id}','HospitalController@update');
Route::delete('hospital/delete/{id}','HospitalController@destroy');

/** Rab Routes  */
Route::get('rabs','RabController@index');
Route::get('rab/create','RabController@create');
Route::post('rab/store','RabController@store');
Route::get('rab/edit/{id}','RabController@edit');
Route::patch('rab/update/{id}','RabController@update');
Route::delete('rab/delete/{id}','RabController@destroy');


/** Police Routes  */
Route::get('polices','PoliceController@index');
Route::get('police/create','PoliceController@create');
Route::post('police/store','PoliceController@store');
Route::get('police/edit/{id}','PoliceController@edit');
Route::patch('police/update/{id}','PoliceController@update');
Route::delete('police/delete/{id}','PoliceController@destroy');


/** Android API Start */
Route::get('/get_divisions','AndroidController@get_divisions');
Route::get('/get_districts/{id}','AndroidController@get_districts');
Route::get('/get_ambulances','AndroidController@get_ambulances');
Route::get('/get_fire_services','AndroidController@get_fire_services');
Route::get('/get_hospitals','AndroidController@get_hospitals');
Route::get('/get_rabs','AndroidController@get_rabs');
Route::get('/get_polices','AndroidController@get_polices');