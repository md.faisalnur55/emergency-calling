<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use App\Police;
use Illuminate\Http\Request;

class PoliceController extends Controller
{
    public function index()
    {
        $polices = Police::all();
        return view('police.index',compact('polices'));
    }

    public function create()
    {
        $divisions = Division::all();
        $districts = District::all();
        return view('police.create',compact('divisions','districts'));
    }

    public function store(Request $request)
    {
        Police::create($request->all());
        $divisions = Division::all();
        $districts = District::all();
        return view('police.create',compact('divisions','districts'));
    }

    public function edit($id)
    {
        $police = Police::query()->findOrFail($id);
        $divisions = Division::all();
        $districts = District::all();
        return view('police.edit',compact('police','divisions','districts'));
    }

    public function update(Request $request,$id)
    {
        $police = Police::query()->findOrFail($id);
        $police->update($request->all());
        redirect('polices');
    }

    public function destroy($id)
    {
        $police = Police::query()->findOrFail($id);
        $police->delete();
        redirect('polices');
    }
}
