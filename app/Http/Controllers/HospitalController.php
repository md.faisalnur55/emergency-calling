<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use App\Hospital;
use Illuminate\Http\Request;

class HospitalController extends Controller
{
    public function index()
    {
        $hospitals = Hospital::all();
        return view('hospital.index',compact('hospitals'));
    }

    public function create()
    {
        $divisions = Division::all();
        $districts = District::all();
        return view('hospital.create',compact('divisions','districts'));
    }

    public function store(Request $request)
    {
        Hospital::create($request->all());
        $divisions = Division::all();
        $districts = District::all();
        return view('hospital.create',compact('divisions','districts'));
    }

    public function edit($id)
    {
        $hospital = Hospital::query()->findOrFail($id);
        $divisions = Division::all();
        $districts = District::all();
        return view('hospital.edit',compact('hospital','divisions','districts'));
    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {

    }
}
