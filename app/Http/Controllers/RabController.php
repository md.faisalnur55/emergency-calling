<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use App\Rab;
use Illuminate\Http\Request;

class RabController extends Controller
{
    public function index()
    {
        $rabs = Rab::all();
        return view('rab.index',compact('rabs'));
    }

    public function create()
    {
        $divisions = Division::all();
        $districts = District::all();
        return view('rab.create',compact('divisions','districts'));
    }

    public function store(Request $request)
    {
        Rab::create($request->all());
        $divisions = Division::all();
        $districts = District::all();
        return view('rab.create',compact('divisions','districts'));
    }

    public function edit($id)
    {
        $rab = Rab::query()->findOrFail($id);
        $divisions = Division::all();
        $districts = District::all();
        return view('rab.edit',compact('rab','divisions','districts'));
    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {

    }
}
