<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function index()
    {
        return view('division.index');
    }

    public function create()
    {
        $divisions = Division::all();
        return view('district.create',compact('divisions'));
    }

    public function store(Request $request)
    {
        District::create($request->all());
        $divisions = Division::all();
        return view('district.create',compact('divisions'));
    }

    public function edit($id)
    {

    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {

    }
}
