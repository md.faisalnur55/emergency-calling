<?php

namespace App\Http\Controllers;

use App\Ambulance;
use App\District;
use App\Division;
use App\FireService;
use App\Hospital;
use App\Police;
use App\Rab;
use Illuminate\Http\Request;

class AndroidController extends Controller
{
    public function get_divisions()
    {
        $divisions = Division::all();
        $division = ["division"=>$divisions];
        return json_encode($division);
    }

    public function get_districts($id)
    {
        $districts = District::query()->where('division_id',$id)->get();
        $district = ["district"=>$districts];
        return json_encode($district);
    }

    public function get_ambulances()
    {
        $ambulances = Ambulance::all();
        $ambulance = ['ambulances'=>$ambulances];
        return json_encode($ambulance);
    }

    public function get_fire_services()
    {
        $fire_services = FireService::all();
        $fire_service = ['fire_services'=>$fire_services];
        return json_encode($fire_service);
    }

    public function get_hospitals()
    {
        $hospitals = Hospital::all();
        $hospital = ['hospitals'=>$hospitals];
        return json_encode($hospital);
    }

    public function get_rabs()
    {
        $rabs = Rab::all();
        $rab = ['rabs'=>$rabs];
        return json_encode($rab);
    }

    public function get_polices()
    {
        $polices = Police::all();
        $police = ['police_stations'=>$polices];
        return json_encode($police);
    }
}
