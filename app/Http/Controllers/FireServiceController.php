<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use App\FireService;
use Illuminate\Http\Request;

class FireServiceController extends Controller
{
    public function index()
    {
        $fire_services = FireService::all();
        return view('fire_service.index',compact('fire_services'));
    }

    public function create()
    {
        $divisions = Division::all();
        $districts = District::all();
        return view('fire_service.create',compact('divisions','districts'));
    }

    public function store(Request $request)
    {
        FireService::create($request->all());
        $divisions = Division::all();
        $districts = District::all();
        return view('fire_service.create',compact('divisions','districts'));
    }

    public function edit($id)
    {
        $fire_service = FireService::query()->findOrFail($id);
        $divisions = Division::all();
        $districts = District::all();
        return view('fire_service.edit',compact('fire_service','divisions','districts'));
    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {

    }
}
