<?php

namespace App\Http\Controllers;

use App\Division;
use Illuminate\Http\Request;

class DivisionController extends Controller
{
    public function index()
    {
        return view('division.index');
    }

    public function create()
    {
        $divisions = Division::all();
        return view('division.create',compact('divisions'));
    }

    public function store(Request $request)
    {
        Division::create($request->all());
        return view('division.create');
    }

    public function edit($id)
    {
        return view('division.create');
    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {
        $division = Division::query()->findOrFail($id);
        $division->delete();
        $divisions = Division::all();
        return view('division.create',compact('divisions'));
    }
}
