<?php

namespace App\Http\Controllers;

use App\Ambulance;
use App\District;
use App\Division;
use Illuminate\Http\Request;

class AmbulanceController extends Controller
{
    public function index()
    {
        $ambulances = Ambulance::all();
        return view('ambulance.index',compact('ambulances'));
    }

    public function create()
    {
        $divisions = Division::all();
        $districts = District::all();
        return view('ambulance.create',compact('divisions','districts'));
    }

    public function store(Request $request)
    {
        Ambulance::create($request->all());
        $divisions = Division::all();
        $districts = District::all();
        return view('ambulance.create',compact('divisions','districts'));
    }

    public function edit($id)
    {
        $ambulance = Ambulance::query()->findOrFail($id);
        $divisions = Division::all();
        $districts = District::all();
        return view('ambulance.edit',compact('ambulance','divisions','districts'));
    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {

    }
}
