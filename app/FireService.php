<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FireService extends Model
{
    protected $fillable = ['name','latitude','longitude','phone','division_id','district_id'];

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }
}
